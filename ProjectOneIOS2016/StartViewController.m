//
//  StartViewController.m
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-15.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "StartViewController.h"

@interface StartViewController ()
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutButton;

@end

@implementation StartViewController


- (void)viewWillLayoutSubviews {
    self.searchButton.alpha = 0.0;
    self.aboutButton.alpha = 0.0;
    
    [UIView animateWithDuration:3 delay:0.1 options:kNilOptions animations:^{
        self.searchButton.alpha = 1.0;
        self.searchButton.transform = CGAffineTransformMakeRotation(M_PI);
        self.searchButton.transform = CGAffineTransformMakeRotation(0);
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:3 delay:0.1 options:kNilOptions animations:^{
            self.aboutButton.alpha = 1.0;
            self.aboutButton.transform = CGAffineTransformMakeRotation(M_PI);
            self.aboutButton.transform = CGAffineTransformMakeRotation(M_PI * 2);
        } completion:^(BOOL finished) {

        }];

    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
