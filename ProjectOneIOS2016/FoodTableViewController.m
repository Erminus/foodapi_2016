//
//  FoodTableViewController.m
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-02.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "FoodTableViewController.h"
#import "FoodTableViewCell.h"
#import "DetailViewController.h"
#import "FoodModel.h"


@interface FoodTableViewController ()
@property(nonatomic)UISearchController *searchController;
@property(nonatomic)FoodModel *foodCollection;

@end

@implementation FoodTableViewController

-(FoodModel *)foodCollection{
    if (!_foodCollection) {
        _foodCollection = [[FoodModel alloc]init];
    }
    return _foodCollection;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchFoodList];
    [self searchFood];
    
}
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSString *searchText = searchController.searchBar.text;
    // Sorting table view data
    NSPredicate *findFoodWithName = [NSPredicate predicateWithFormat:@"name contains[c] %@",searchText];
    self.foodCollection.searchedFood = [self.foodCollection.allFoodList filteredArrayUsingPredicate:findFoodWithName];
    [self.tableView reloadData];
}

-(void)searchFood{
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    // Placing searchbar
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self.searchController loadViewIfNeeded];
}
-(void)fetchFoodList{
    
    NSString *url = [NSString stringWithFormat:@"http://matapi.se/foodstuff"];
    NSString *escaped = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    //Url
    NSURL *foodUrl = [NSURL URLWithString:escaped];
    //Request
    NSURLRequest *foodRequest = [NSURLRequest requestWithURL:foodUrl];
    // Session
    NSURLSession *foodSession = [NSURLSession sharedSession];
    // Sorting array
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    // DataTask
    NSURLSessionDataTask *foodTask = [foodSession dataTaskWithRequest:foodRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error %@",error);
            return;
        }
        
        NSError *jsonErrorParse = nil;
        
        self.foodCollection.allFoodList = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonErrorParse];
        // Sorting array of foods
        self.foodCollection.allFoodList = [self.foodCollection.allFoodList sortedArrayUsingDescriptors:sortDescriptors];
        self.foodCollection.searchedFood = [self.foodCollection.searchedFood sortedArrayUsingDescriptors:sortDescriptors];
        
        if (jsonErrorParse) {
            NSLog(@"Failed to parse json %@",jsonErrorParse);
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
    [foodTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        return self.foodCollection.searchedFood.count;
    } else {
        return self.foodCollection.allFoodList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FoodCell" forIndexPath:indexPath];
    NSArray *activeFood;
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        activeFood = self.foodCollection.searchedFood;
    } else {
        activeFood = self.foodCollection.allFoodList;
    }
    // Custom text on cell
    cell.textLabel.text = activeFood[indexPath.row][@"name"];
    cell.foodInformation = activeFood[indexPath.row];

    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
    [cell.contentView.layer setBorderColor:[UIColor grayColor].CGColor];
    [cell.contentView.layer setBorderWidth:0.4f];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    FoodTableViewCell *cell = sender;
    DetailViewController *details = segue.destinationViewController;
    details.title = [NSString stringWithFormat:@"%@",cell.foodInformation[@"name"]];
    details.foodNumberText = [NSString stringWithFormat:@"%@",cell.foodInformation[@"number"]];

}


@end
