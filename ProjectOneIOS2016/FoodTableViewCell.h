//
//  FoodTableViewCell.h
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-08.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewCell : UITableViewCell

@property(nonatomic)NSDictionary *foodInformation;

@end
