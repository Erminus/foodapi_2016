//
//  AboutViewController.m
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-15.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@property (nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic) IBOutlet UIImageView *cookView;
@property (nonatomic)UIDynamicAnimator *animator;
@property (nonatomic)UIGravityBehavior *gravity;
@property (nonatomic)UICollisionBehavior *collision;
@end

@implementation AboutViewController

- (IBAction)goBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self animateCookView];
}

-(void)animateCookView{
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    [self itemCollision];
    [self itemGravity];
}

-(void)itemGravity{
    self.gravity = [[UIGravityBehavior alloc]initWithItems:@[self.cookView]];
    
    [self.animator addBehavior:self.gravity];
}

-(void)itemCollision{
    self.collision = [[UICollisionBehavior alloc]initWithItems:@[self.cookView]];
    [self.collision addBoundaryWithIdentifier:@"bottom" fromPoint:CGPointMake(self.view.frame.origin.x,CGRectGetMaxY(self.view.frame) - self.backButton.bounds.size.height * 3.5) toPoint:CGPointMake(self.view.frame.size.width,(CGRectGetMaxY(self.view.frame) - self.backButton.bounds.size.height * 3.5))];
    [self.animator addBehavior:self.collision];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
