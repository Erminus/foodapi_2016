//
//  FoodModel.m
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-10.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "FoodModel.h"

@implementation FoodModel

-(NSArray *)vitaminValues{
    if (!_vitaminValues) {
        _vitaminValues = @[@([self.foodValueList[@"nutrientValues"][@"vitaminB12"]floatValue]),
                           @([self.foodValueList[@"nutrientValues"][@"vitaminB6"]floatValue]),
                           @([self.foodValueList[@"nutrientValues"][@"vitaminC"]floatValue]),
                           @([self.foodValueList[@"nutrientValues"][@"vitaminE"]floatValue])
                           ];
    }
    return _vitaminValues;
}

// Calculate all vitamins nutrient Values
+(float)calculateValues:(NSArray *)array{
    float sum = 0;
    for (NSString *values in array) {
        sum += [values floatValue];
    }
    return sum;
}
// Storing all food
-(NSArray *)allFoodList{
    if (!_allFoodList) {
        _allFoodList = @[].mutableCopy;
    }
    return _allFoodList;
}

// Storing searched list
-(NSArray *)searchedFood{
    if (!_searchedFood) {
        _searchedFood = @[].mutableCopy;
    }
    return _searchedFood;
}

// Storing all value for the food name
-(NSDictionary *)foodValueList{
    if (!_foodValueList) {
        _foodValueList = @{}.mutableCopy;
    }
    return _foodValueList;
}


@end
