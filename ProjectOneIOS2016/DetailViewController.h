//
//  DetailViewController.h
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-08.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

// Value properties
@property (weak, nonatomic) IBOutlet UILabel *proteinValue;
@property (weak, nonatomic) IBOutlet UILabel *fatValue;
@property (weak, nonatomic) IBOutlet UILabel *vitB12Value;
@property (weak, nonatomic) IBOutlet UILabel *vitB6Value;
@property (weak, nonatomic) IBOutlet UILabel *vitCValue;
@property (weak, nonatomic) IBOutlet UILabel *vitEValue;
@property (weak, nonatomic) IBOutlet UILabel *vitaminSammary;

@property(nonatomic)NSString *foodNumberText;

@end
