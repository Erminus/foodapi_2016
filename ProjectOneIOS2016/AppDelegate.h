//
//  AppDelegate.h
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-02-23.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

