//
//  FoodModel.h
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-10.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodModel : NSObject

@property(nonatomic)NSArray *allFoodList;
@property(nonatomic)NSArray *searchedFood;
@property(nonatomic)NSDictionary *foodValueList;
@property(nonatomic)NSArray *vitaminValues;

+(float)calculateValues:(NSArray *)array;

@end
