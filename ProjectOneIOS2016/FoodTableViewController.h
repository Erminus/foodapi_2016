//
//  FoodTableViewController.h
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-02.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewController : UITableViewController <UISearchResultsUpdating>

// Called when the search bar's text or scope has changed or when the search bar becomes first responder.
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController;

@end
