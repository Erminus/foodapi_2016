//
//  DetailViewController.m
//  ProjectOneIOS2016
//
//  Created by Ermin on 2016-03-08.
//  Copyright © 2016 Ermin Mahmutovic. All rights reserved.
//

#import "DetailViewController.h"
#import "FoodTableViewCell.h"
#import "FoodModel.h"

@interface DetailViewController ()
@property(nonatomic)FoodModel *foodDetails;

@end

@implementation DetailViewController

-(FoodModel *)foodDetails{
    if (!_foodDetails) {
        _foodDetails = [[FoodModel alloc]init];
    }
    return _foodDetails;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchFoodDetails];
}

-(void)fetchFoodDetails{

    NSString *url2 = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@",self.foodNumberText];
    NSString *escaped2 = [url2 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    //Url
    NSURL *foodUrl2 = [NSURL URLWithString:escaped2];
    //Request
    NSURLRequest *foodRequest2 = [NSURLRequest requestWithURL:foodUrl2];
    // Session
    NSURLSession *foodSession2 = [NSURLSession sharedSession];
    // DataTask
    NSURLSessionDataTask *foodTask2 = [foodSession2 dataTaskWithRequest:foodRequest2 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error %@",error);
            return;
        }
        NSError *jsonErrorParse = nil;
        
        self.foodDetails.foodValueList = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonErrorParse];
        
        if (jsonErrorParse) {
            NSLog(@"Failed to parse json %@",jsonErrorParse);
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.proteinValue.text = [NSString stringWithFormat:@"%.02f g",[self.foodDetails.foodValueList[@"nutrientValues"][@"protein"]floatValue]];
            self.fatValue.text = [NSString stringWithFormat:@"%.02f g",[self.foodDetails.foodValueList[@"nutrientValues"][@"fat"]floatValue]];
            self.vitB12Value.text = [NSString stringWithFormat:@"%.02f g",[self.foodDetails.foodValueList[@"nutrientValues"][@"vitaminB12"]floatValue]];
            self.vitB6Value.text = [NSString stringWithFormat:@"%.02f g",[self.foodDetails.foodValueList[@"nutrientValues"][@"vitaminB6"]floatValue]];
            self.vitCValue.text = [NSString stringWithFormat:@"%.02f g",[self.foodDetails.foodValueList[@"nutrientValues"][@"vitaminC"]floatValue]];
            self.vitEValue.text = [NSString stringWithFormat:@"%.02f g",[self.foodDetails.foodValueList[@"nutrientValues"][@"vitaminE"]floatValue]];
            
            self.vitaminSammary.text = [NSString stringWithFormat:@"%.02f g", [FoodModel calculateValues:self.foodDetails.vitaminValues]];
        });
    }];
    [foodTask2 resume];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
